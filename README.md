#Lecteur de flux de syndication
Auteur : Delétoille Antoine, Loukhal Maxime

Ce projet contient quatre pages :
- Page d'accueil/inscription (index.html)
- Page de connexion (connexion.html)
- Page de lecture de flux (flux.html)
- Page d'ajout d'un flux (newflux.html)

Pour le test :
email : test@gmail.com
password : Test

L'ensemble du projet est fonctionnel.

L'affichage de flux ce fait avec des cards (Bootstrap)
Une card a le titre du flux général et son contenu sont plusieurs cards qui contiennent les informations de chaque article du flux.
Nous n'avons pas réussi à afficher le "pubDate".

Le projet a été réalisé en python 3.8 à l'aide de flask et jinja2 en grande partie, l'environnement virtual comprend : 

1. pytest | 5.3.5  
2. flask  | 1.1.1 
3. jinja2 | 2.11.1
4. requests| 2.23.0
5. peewee | 3.13.1 
6. faker  | 4.0.1 
7. awesome-slugify | 1.6.5
8. flask-wtf | 0.14.3
9. wtf-peewee | 3.0.0
10. bcrypt | 3.1.7
11. feedparser | 5.2.1
12. lxml | 4.5.0 | 
13. flask-login | 0.5.0 








